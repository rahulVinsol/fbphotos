//
//  Model.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 18/06/21.
//

import Foundation

// MARK: - Photos
struct Photos: Codable {
    let data: [Datum]
    let paging: Paging
}

// MARK: - Datum
struct Datum: Codable {
    let images: [ImageModel]
    let id: String
}

// MARK: - Image
struct ImageModel: Codable {
    let height: Int
    let source: String
    let width: Int
}

// MARK: - Paging
struct Paging: Codable {
    let cursors: Cursors
}

// MARK: - Cursors
struct Cursors: Codable {
    let before, after: String
}
