//
//  DefaultsHelper.swift
//  FBPhotosStory
//
//  Created by Rahul Rawat on 20/06/21.
//

import Foundation

class DefaultsHelper {
    static var shared: DefaultsHelper = DefaultsHelper()
    
    private let loggedKey = "LOGGED_KEY"
    private let emailKey = "LOGGED_EMAIL"
    private let userIdKey = "USER_ID_KEY"
    
    private let defaults = UserDefaults.standard
    
    private init() { }
    
    func set(isLoggedIn value: Bool) {
        defaults.setValue(value, forKey: loggedKey)
    }
    
    func set(email value: String) {
        defaults.setValue(value, forKey: emailKey)
    }
    
    func set(userId value: String) {
        defaults.setValue(value, forKey: userIdKey)
    }
    
    func isLoggedIn() -> Bool {
        defaults.bool(forKey: loggedKey)
    }
    
    func getEmail() -> String {
        defaults.string(forKey: emailKey) ?? ""
    }
    
    func getUserId() -> String {
        defaults.string(forKey: userIdKey) ?? ""
    }
    
    func clearAll() {
        defaults.removeObject(forKey: loggedKey)
        defaults.removeObject(forKey: emailKey)
        defaults.removeObject(forKey: userIdKey)
    }
}
