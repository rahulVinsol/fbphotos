//
//  ImageDownloaderUtility.swift
//  FacebookPhotos
//
//  Created by Rahul Rawat on 19/06/21.
//

import AlamofireImage

class ImageDownloadUtility {
    static let shared = ImageDownloadUtility()
    
    private var downloader: ImageDownloader!
    
    private init() {
        downloader = ImageDownloader(
            configuration: ImageDownloader.defaultURLSessionConfiguration(),
            downloadPrioritization: .lifo,
            maximumActiveDownloads: 8,
            imageCache: AutoPurgingImageCache()
        )
    }
    
    func download(url: String, completionHandler: @escaping (UIImage) -> Void) {
        let urlRequest = URLRequest(url: URL(string: url)!)
        
        downloader.download(urlRequest, completion:  { response in
            if case .success(let image) = response.result {
                completionHandler(image)
            }
        })
    }
}
