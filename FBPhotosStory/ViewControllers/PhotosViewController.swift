//
//  PhotosViewController.swift
//  FBPhotosStory
//
//  Created by Rahul Rawat on 21/06/21.
//

import UIKit

class PhotosViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    
    var images: [ImageModel]!
    private var indexArr = [Int]()
    private var scrollViewOriginalFrame: CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let count = images.count
        indexArr.append(max(count - 2, 0))
        indexArr.append(count - 1)
        indexArr.append(0)
        indexArr.append(min(1, count - 1))
        indexArr.append(count < 3 ? 0 : 2)
        
        let midIndex = stackView.subviews.count / 2
        
        self.view.layoutIfNeeded()
        scrollView.setContentOffset(CGPoint(x: CGFloat(midIndex) * Screen.width, y: CGFloat(0)), animated: false)
        scrollView.delegate = self
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 4.0
        scrollView.zoomScale = 1.0
        
        scrollViewOriginalFrame = scrollView.frame
        
        if images.count <= 1 {
            scrollView.isScrollEnabled = false
        }
        
        let sortedSubviews = stackView.subviews.sorted(by: { firstView, secondView in
            firstView.frame.origin.x < secondView.frame.origin.x
        })
        
        for index in 0..<sortedSubviews.count {
            let view = sortedSubviews[index]
            if let imageView = view as? UIImageView {
                let indexToPull = indexArr[index]
                imageView.updateImage(url: images[indexToPull].source)
            }
        }
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(recognizer:)))
        doubleTap.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTap)
    }
    
    @objc func handleDoubleTap(recognizer: UITapGestureRecognizer) {
        let midCount = CGFloat(stackView.subviews.count / 2)
        
        if (scrollView.zoomScale > scrollView.minimumZoomScale) {
            scrollView.contentSize = CGSize(width: stackView.frame.width, height: stackView.frame.height)
            
            let zoomToRect = CGRect(x: 0, y: 0, width: scrollView.frame.width, height: scrollView.frame.height)
            scrollView.zoom(to: zoomToRect, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.scrollView.setContentOffset(CGPoint(x: midCount * CGFloat(self.scrollView.frame.width), y: 0), animated: false)
                self.stackView.bounds.origin = CGPoint(x: 0, y: 0)
            }
        } else {
            let touchPoint = recognizer.location(in: stackView)
            
            stackView.bounds.origin = CGPoint(x: midCount * CGFloat(scrollView.frame.width), y: 0)
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            let zoomScale = scrollView.maximumZoomScale
            let zoomScaleTwice = zoomScale * 2
            
            var newX = touchPoint.x - scrollView.frame.width / zoomScaleTwice
            var newY = touchPoint.y - scrollView.frame.height / zoomScaleTwice
            let newWidth = scrollView.frame.width / zoomScale
            let newHeight = scrollView.frame.height / zoomScale
            
            let zoomWidth = scrollView.frame.width * midCount
            if newX < zoomWidth {
                newX = zoomWidth
            } else if newX + newWidth > zoomWidth + scrollView.frame.width {
                newX = zoomWidth + scrollView.frame.width - newWidth
            }
            
            if newY < 0 {
                newY = 0
            } else if newY + newHeight > scrollView.frame.height {
                newY = scrollView.frame.height - newHeight
            }
            
            let zoomToRect = CGRect(x: newX, y: newY, width: newWidth, height: newHeight)
            scrollView.zoom(to: zoomToRect, animated: true)
            
            scrollView.contentSize = CGSize(width: scrollView.frame.width * zoomScale, height: scrollView.frame.height * zoomScale)
        }
        
        scrollView.isPagingEnabled = scrollView.zoomScale == 1
    }
    
    func update(imageView: UIImageView, withImageAt index: Int) {
        ImageDownloadUtility.shared.download(url: images[index].source) { image in
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
        }
    }
}

extension PhotosViewController: UIScrollViewDelegate {
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        scrollingEnd(scrollView)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        scrollingEnd(scrollView)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return stackView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        if scrollView.zoomScale > 1 {
            let zoomScale = scrollView.zoomScale
            let imageView = scrollView.subviews[0].subviews[2] as? UIImageView
            if let imageView = imageView, let image = imageView.image {
                let ratioW = imageView.frame.width / image.size.width
                let ratioH = imageView.frame.height / image.size.height
                
                let ratio = ratioW < ratioH ? ratioW : ratioH
                let newWidth = image.size.width * ratio
                let newHeight = image.size.height * ratio
                let conditionLeft = newWidth*scrollView.zoomScale > imageView.frame.width
                let left =  zoomScale * 0.5 * (conditionLeft ? newWidth - imageView.frame.width : (scrollView.frame.width - scrollView.contentSize.width))
                let conditioTop = newHeight*scrollView.zoomScale > imageView.frame.height
                
                let top = zoomScale * 0.5 * (conditioTop ? newHeight - imageView.frame.height : (scrollView.frame.height - scrollView.contentSize.height))
                
                scrollView.contentInset = UIEdgeInsets(top: top, left: left, bottom: top, right: left)
                
            }
        } else {
            scrollView.contentInset = .zero
        }
    }
    
    private func scrollingEnd(_ scrollView: UIScrollView) {
        if scrollView.zoomScale != 1 {
            return
        }
        
        let hStack = scrollView.subviews[0]
        
        let currentPos = Int(scrollView.contentOffset.x / Screen.width)
        let subViewCount = hStack.subviews.count
        
        if currentPos == subViewCount / 2 {
            return
        }
        
        var sortedSubviews = hStack.subviews.sorted(by: { firstView, secondView in
            firstView.frame.origin.x < secondView.frame.origin.x
        })
        
        let posToRemove: Int
        let newInsertionIndex: Int
        
        let imageCount = images.count
        
        let isGoingLeft = subViewCount - currentPos > currentPos
        
        var indexElementToChange: Int
        
        if isGoingLeft {
            posToRemove = subViewCount - 1
            newInsertionIndex = 0
            
            indexElementToChange = indexArr[0] - 1
            indexArr.removeLast()
            
            indexElementToChange = indexElementToChange < 0 ? imageCount - 1 : indexElementToChange
            indexArr.insert(indexElementToChange, at: 0)
        } else {
            posToRemove = 0
            newInsertionIndex = subViewCount - 1
            
            indexElementToChange = indexArr[4] + 1
            indexArr.removeFirst()
            
            indexElementToChange = indexElementToChange < imageCount ? indexElementToChange : 0
            indexArr.append(indexElementToChange)
        }
        
        let removedView = sortedSubviews.remove(at: posToRemove)
        sortedSubviews.insert(removedView, at: newInsertionIndex)
        
        (removedView as? UIImageView)?.updateImage(url: images[indexElementToChange].source)
        
        for index in 0..<subViewCount {
            let viewToAdd = sortedSubviews[index]
            
            viewToAdd.removeFromSuperview()
            
            viewToAdd.heightAnchor.constraint(equalToConstant: stackView.frame.height).isActive = true
            viewToAdd.widthAnchor.constraint(equalToConstant: scrollView.frame.width).isActive = true
            
            stackView.addArrangedSubview(viewToAdd)
        }
        
        scrollView.setContentOffset(CGPoint(x: CGFloat(subViewCount / 2) * Screen.width, y: 0), animated: false)
    }
}

extension UIImageView {
    func updateImage(url: String) {
        ImageDownloadUtility.shared.download(url: url) {[weak self] image in
            self?.image = image
        }
    }
}

struct Screen {
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
}
