//
//  ViewController.swift
//  FBPhotosStory
//
//  Created by Rahul Rawat on 19/06/21.
//

import UIKit
import FBSDKLoginKit

class ViewController: UIViewController {
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var buttonPhotos: UIButton!
    @IBOutlet weak var labelInfo: UILabel!

    private var defaultHelper: DefaultsHelper!
    
    private var logged = false
    private var email = ""
    private var userId = ""
    
    var manager = LoginManager()
    var images: [ImageModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.defaultHelper = DefaultsHelper.shared
        
        logged = defaultHelper.isLoggedIn()
        email = defaultHelper.getEmail()
        userId = defaultHelper.getUserId()
        
        loginButton.layer.cornerRadius = 15
        buttonPhotos.layer.cornerRadius = 15
        
        buttonPhotos.isHidden = true
        loginButton.setTitle(logged ? "Log out" : "Login", for: .normal)
        
        if logged {
            labelInfo.text = email
            self.getPhotos()
        }
    }

    @IBAction func loginPressed(_ sender: UIButton) {
        if !logged {
            showFacebookLogin()
        } else {
            manager.logOut()
            email = ""
            logged = false
            userId = ""
            images.removeAll()
            
            defaultHelper.clearAll()
            
            loginButton.setTitle("Login", for: .normal)
            buttonPhotos.isHidden = true
            labelInfo.text = ""
        }
    }
    
    private func showFacebookLogin() {
        manager.logIn(permissions: ["public_profile", "email", "user_photos", "user_posts"], from: nil) {[weak self] (result, err) in
            guard let self = self else { return }
            
            if err != nil {
                print(err!.localizedDescription)
                return
            }
            
            if result?.isCancelled == false {
                self.logged = true
                self.defaultHelper.set(isLoggedIn: self.logged)
                
                self.loginButton.setTitle("Log out", for: .normal)
                
                let request = GraphRequest(graphPath: "me", parameters: ["fields" : "email"])
                
                request.start { (_, res, _) in
                    guard let profileData = res as? [String : Any] else { return }
                    
                    self.email = profileData["email"] as! String
                    self.userId = profileData["id"] as! String
                    
                    self.defaultHelper.set(email: self.email)
                    self.defaultHelper.set(userId: self.userId)
                    
                    self.labelInfo.text = self.email
                    
                    self.getPhotos()
                }
            }
        }
    }
    
    private func getPhotos() {
        let request = GraphRequest(graphPath: "\(userId)/photos/uploaded", parameters: ["fields" : "images"])
        request.start { [weak self] _, res, _ in
            guard let self = self, let profileData = res as? [String : Any] else { return }
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: profileData, options: .prettyPrinted)
                
                let photos = try JSONDecoder().decode(Photos.self, from: Data(jsonData))
                
                self.images = [ImageModel]()
                for photo in photos.data {
                    if let image = photo.images.first {
                        self.images.append(image)
                    }
                }
                
                if !self.images.isEmpty {
                    self.buttonPhotos.isHidden = false
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPhotos" {
            let pvc = segue.destination as! PhotosViewController
            pvc.images = images
        }
    }
}
